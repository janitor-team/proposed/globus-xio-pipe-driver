globus-xio-pipe-driver (4.1-3) unstable; urgency=medium

  * Drop old debug symbol migration from 2017

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Tue, 05 Jul 2022 22:46:12 +0200

globus-xio-pipe-driver (4.1-2) unstable; urgency=medium

  * Change to debhelper compat level 13
  * Remove override_dh_missing rule (--fail-missing is default)
  * Drop ancient Replaces/Conflicts/Breaks

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Tue, 15 Dec 2020 21:53:38 +0100

globus-xio-pipe-driver (4.1-1) unstable; urgency=medium

  * Add AC_CONFIG_MACRO_DIR and ACLOCAL_AMFLAGS

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Tue, 03 Sep 2019 05:06:32 +0200

globus-xio-pipe-driver (4.0-2) unstable; urgency=medium

  * Convert debian/rules to dh tool
  * Change to debhelper compat level 10
  * Update documentation links in README file

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Fri, 12 Jul 2019 16:34:52 +0200

globus-xio-pipe-driver (4.0-1) unstable; urgency=medium

  * Switch upstream to Grid Community Toolkit
  * First Grid Community Toolkit release
  * Move VCS to salsa.debian.org

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Sun, 16 Sep 2018 03:16:32 +0200

globus-xio-pipe-driver (3.10-2) unstable; urgency=medium

  * Migrate to dbgsym packages

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Thu, 06 Jul 2017 12:58:38 +0200

globus-xio-pipe-driver (3.10-1) unstable; urgency=medium

  * GT6 update: Fix .pc typo
  * Drop dummy package libglobus-xio-pipe-driver0

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Tue, 27 Jun 2017 11:45:30 +0200

globus-xio-pipe-driver (3.9-1) unstable; urgency=medium

  * GT6 update
  * Change Maintainer e-mail (fysast → physics)

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Fri, 02 Sep 2016 16:41:54 +0200

globus-xio-pipe-driver (3.8-1) unstable; urgency=medium

  * GT6 update
  * Replace ldconfig postinst/postrm scripts with triggers

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 03 May 2016 18:24:52 +0200

globus-xio-pipe-driver (3.7-2) unstable; urgency=medium

  * Update URLs (use toolkit.globus.org)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 16 Feb 2016 17:30:04 +0100

globus-xio-pipe-driver (3.7-1) unstable; urgency=medium

  * Update to Globus Toolkit 6.0
  * Drop GPT build system and GPT packaging metadata
  * Rename binary package due to dropped soname (this is a plugin)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 23 Sep 2014 17:07:21 +0200

globus-xio-pipe-driver (2.2-3) unstable; urgency=low

  * Implement Multi-Arch support
  * Rename dbg package

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sun, 10 Nov 2013 20:33:23 +0100

globus-xio-pipe-driver (2.2-2) unstable; urgency=low

  * Add arm64 to the list of 64 bit architectures

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sun, 26 May 2013 17:36:55 +0200

globus-xio-pipe-driver (2.2-1) unstable; urgency=low

  * Update to Globus Toolkit 5.2.1
  * Drop patches globus-xio-pipe-driver-funcgrp.patch and
    globus-xio-pipe-driver-deps.patch (fixed upstream)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sun, 29 Apr 2012 11:56:57 +0200

globus-xio-pipe-driver (2.1-2) unstable; urgency=low

  * Fix broken links in README file

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 24 Jan 2012 18:53:31 +0100

globus-xio-pipe-driver (2.1-1) unstable; urgency=low

  * Update to Globus Toolkit 5.2.0
  * Drop patch globus-xio-pipe-driver-wrong-dep.patch (fixed upstream)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 28 Dec 2011 16:53:53 +0100

globus-xio-pipe-driver (0.1-4) unstable; urgency=low

  * Fix FTBFS with new dpkg (Closes: #643483)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Mon, 03 Oct 2011 07:17:25 +0200

globus-xio-pipe-driver (0.1-3) unstable; urgency=low

  * Add README file
  * Clear dependency libs in .la file (Closes: #621168)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 26 Apr 2011 13:34:33 +0200

globus-xio-pipe-driver (0.1-2) unstable; urgency=low

  * Converting to package format 3.0 (quilt)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Fri, 04 Jun 2010 04:43:30 +0200

globus-xio-pipe-driver (0.1-1) unstable; urgency=low

  * Initial release (Closes: #567121)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 27 Jan 2010 14:53:08 +0100
